#!/bin/bash -l

apt update && apt install -y ntp mc htop screen wget docker.io docker docker-compose build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libxslt1-dev libxml2-dev git curl mysql-client libmysqlclient-dev python3-dev
systemctl enable docker
service ntp restart

cd /root/react-django-example/app/backend/
docker build -t django-app-example:latest .

cd ../frontend/

# change proxy for package-json (frontend-build)
sed -i -e "s/localhost/$BACKEND_SERVER/" package.json

docker build -t react-app-example:latest .

cd ../../docker/

docker-compose down
docker-compose kill
docker volume prune


cp -f server.docker-compose.yml docker-compose.yml
#[ -f ./init.sh ] && ./init.sh || exit 1
docker-compose up -d --build --force-recreate --remove-orphans
#[ -f ./post_init.sh ] && ./post_init.sh || exit 1
