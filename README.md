# React + Django example

## Idea

This is an example of web service with online notebook. You can create, edit and delete notes. All notes are shown
in the home page with date of creation and title (45 first characters from a note).

## How to use

Enter [```http://62.113.115.105:30001/```](http://62.113.115.105:30001/) in your browser to go to [Home Page](http://62.113.115.105:30001/). Press [```+```](http://62.113.115.105:30001/note/new) button [to add a new Note](http://62.113.115.105:30001/note/new). 
Go to Note, edit text and back to [Home Page](http://62.113.115.105:30001/) to edit the Note. To delete Note, erase all text in the Note and back to 
Home Page, or press ```Delete``` button.

## Routing

```http://62.113.115.105:30001/``` - Home Page with Notes List   
```http://62.113.115.105:30001/note/new``` - Create a new Note  
```http://62.113.115.105:30001/note/:id``` - Open Note with specified id  

## API

App has Restful API. To get API list, go to [```http://62.113.115.105:8090/api/```](http://62.113.115.105:8090/api/)

### Endpoints

Base Backend URL - [```http://62.113.115.105:8090/api```](http://62.113.115.105:8090/api/)

[```"/notes/"```](http://62.113.115.105:8090/api/notes/) - Returns an _array of notes_ (**GET** Method)  
[```"/notes/id"```](http://62.113.115.105:8090/api/notes/id) - Returns a _single note_ object (**GET** Method)  
[```"/notes/create"```](http://62.113.115.105:8090/api/notes/create) - _Create new note_ with data sent in post request (**POST** Method)  
[```"/notes/id/update/"```](http://62.113.115.105:8090/api/notes/id/update/) - _Creates an existing note_ with data sent in post request (**PUT** Method)  
[```"/notes/id/delete/"```](http://62.113.115.105:8090/api/notes/id/delete/) - _Deletes_ and exiting note (**DELETE** Method)  

## Architecture

### Stack of Technologies

* Docker
* Django Python
* React
* CSS
* Gitlab CI/CD

### Description

App has **backend** written on Django and **frontend** written on React. 


Project has partial Model-View-Controller model. 
Built-in SQLLite DataBase from Django is used to store data. To work with database Model application from Django is used.
There are no views in Django just to use React as frontend. To make API-backend ```rest_framework``` library from Django is
used. Backend has own Dockerfile to create Docker container with Django and backend API.


Frontend works with React. To go through pages, _Routing_ library is used. All ```.svg``` - pictures and CSS files stores in special folder.
Frontend has own Dockerfile to create Docker container with React and files to run ```npm```-server.


All Docker containers are run in common Docker network. Rules to run Dockers are described in server.docker-compose.yml


Script restart.sh re-build images for backend and frontend and run up docker-compose. Script is called after each commit (to see more information - read about CI/CD)
